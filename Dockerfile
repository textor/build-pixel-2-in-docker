FROM ubuntu:14.04
ARG userid
ARG groupid
ENV username pixel

RUN mkdir /home/${username}

ENV ARCH arm64 
ENV SUBARCH arm64 
ENV CROSS_COMPILE aarch64-linux-android-
ENV AOSP /home/${username}/AOSP
ENV CLANG_TRIPLE aarch64-linux-gnu-
ENV CLANG_PREBUILT_BIN ${AOSP}/prebuilts/clang/host/linux-x86/clang-4053586/bin
ENV CC_CMD ${CLANG_PREBUILT_BIN}/clang
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
ENV PATH="${JAVA_HOME}/bin:/home/${username}/bin:$AOSP/out/host/linux-x86/bin:/home/${username}/aarch64-linux-android-4.9/bin:$PATH"


RUN sed --in-place --regexp-extended "s/(\/\/)(archive\.ubuntu)/\1nl.\2/" /etc/apt/sources.list

RUN apt-get update && apt-get install -y git-core gnupg flex bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip python liblz4-tool libssl-dev wget bc

# openjdk-7-jdk

RUN curl -o jdk8.tgz "https://android.googlesource.com/platform/prebuilts/jdk/jdk8/+archive/master.tar.gz" \
    && tar -zxf jdk8.tgz linux-x86 \
    && mkdir -p /usr/lib/jvm/ \
    && mv linux-x86 /usr/lib/jvm/java-8-openjdk-amd64 \
    && rm -rf jdk8.tgz

RUN mkdir /home/${username}/bin \
    && cd /home/${username}/bin \
    && curl https://raw.githubusercontent.com/esrlabs/git-repo/stable/repo > repo \
    && chmod a+x ./repo

RUN git config --global user.email "pixel2@example.com" && \
    git config --global user.name "pixel2"

# Minimalistic AOSP

RUN cd /home/${username} && mkdir AOSP && cd AOSP \
    && repo init -u https://github.com/nathanchance/pixel2-manifest -b 8.1.0 \
    && repo sync -j$(nproc --all)

# NDK

RUN cd /home/${username} && git clone https://android.googlesource.com/platform/prebuilts/gcc/linux-x86/aarch64/aarch64-linux-android-4.9

# Building tools

RUN cd ${AOSP} && /bin/bash -c ". build/envsetup.sh && lunch aosp_walleye-userdebug && make dtc && make mkdtimg"
