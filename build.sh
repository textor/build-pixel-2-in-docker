#!/bin/bash


apply_config_patch() {
    sed 's/CONFIG_MODULES=n/CONFIG_MODULES=y/g' -i .config
    sed 's/# CONFIG_DEVMEM is not set/CONFIG_DEVMEM=y/g' -i .config
    sed 's/# CONFIG_SECURITY_SELINUX_DISABLE is not set/CONFIG_SECURITY_SELINUX_DISABLE=y/g' -i .config    
    sed 's/CONFIG_MODULE_UNLOAD=n/CONFIG_MODULE_UNLOAD=y/g' -i .config
    sed 's/# CONFIG_DEVKMEM is not set/CONFIG_DEVKMEM=y/g' -i .config
    echo "[+] Config was patched to support LKM"
}


ENABLE_PATCH=0
KERNEL_GIT_REPO=$PWD
CONTAINER_GIT_REPO="/home/pixel/msm"

for i in "$@"
do
case $i in
    -p|--patch)    
    ENABLE_PATCH=1
    shift # past argument=value
    ;;
    --kernel=*)    
    KERNEL_GIT_REPO="${i#*=}"
    shift
    ;;
    *)
    ;;
esac
done

echo "[~] Kernel sources at ${KERNEL_GIT_REPO}"
cd ${KERNEL_GIT_REPO}

docker run -t --rm -v ${KERNEL_GIT_REPO}:${CONTAINER_GIT_REPO}:rw pixel2\
     /bin/bash -c "cd ${CONTAINER_GIT_REPO} && make wahoo_defconfig"
 
if [ $? -eq 0 ]; then
    echo "[+] Builded default config"    
else
    echo "[~] Failde to build config"
    exit
fi

if [ $ENABLE_PATCH == 1 ]; then
    echo "[~] Patch enabled : lkm support"    
    apply_config_patch    
fi

docker run -t --rm -v ${KERNEL_GIT_REPO}:${CONTAINER_GIT_REPO}:rw pixel2\
     /bin/bash -c "cd ${CONTAINER_GIT_REPO} && make CC=\${CC_CMD}"

echo "[+] Kernel was builded"

docker run -t --rm -v ${KERNEL_GIT_REPO}:${CONTAINER_GIT_REPO}:rw pixel2\
     /bin/bash -c "cd ${CONTAINER_GIT_REPO} && \
     cp arch/arm64/boot/Image.lz4-dtb \$AOSP/device/google/wahoo-kernel/Image.lz4-dtb && \
     cp arch/arm64/boot/dtbo.img \$AOSP/device/google/wahoo-kernel/dtbo.img && \
     cp drivers/input/touchscreen/stm/*.ko \$AOSP/device/google/wahoo-kernel && \
     cp drivers/power/*.ko \$AOSP/device/google/wahoo-kernel && \
     cp drivers/input/touchscreen/synaptics_dsx_htc/*.ko \$AOSP/device/google/wahoo-kernel && \
     cp drivers/input/touchscreen/lge/*.ko \$AOSP/device/google/wahoo-kernel && \
     cp drivers/input/touchscreen/lge/lgsic/*.ko \$AOSP/device/google/wahoo-kernel && \
     cd \$AOSP && . build/envsetup.sh && lunch aosp_walleye-userdebug && make bootimage -j4 && mkdir -p ${CONTAINER_GIT_REPO}/builded_images && \
     cp out/target/product/walleye/*.img ${CONTAINER_GIT_REPO}/builded_images"


echo "Builded images:"
ls "${KERNEL_GIT_REPO}/builded_images/*.img"